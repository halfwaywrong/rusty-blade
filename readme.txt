This has been put together by me, Michael Henderson aka HalfwayWrong for A Game By It's Cover Jam 2017.

Everything here, including my code, is public domain so feel free to do what you want with it. If you do find something useful here, or even have some feedback, let me know, I'd love to hear it.

Apologies in advance for some of the code - I chose speed at the expense of quality :P.

It's made with Godot Engine 2.1. My favourite engine for 2D game dev, I can't recommend it highly enough!

Some code is reappropriated from Godot's platformer example. All art and sound is public domain. I've misplaced the source for some of the assets, but here's where most of them can be found. If you notice any I've missed, let me know and I'll add it.

https://opengameart.org/content/another-nes-like-village
https://opengameart.org/content/mini-knight
https://opengameart.org/content/mini-knight-expansion-1
https://opengameart.org/content/rpgstringsspooky
https://godotengine.org/qa/438/camera2d-screen-shake-extension
https://opengameart.org/content/chick
http://soundbible.com/871-Chicken.html
https://fontlibrary.org/en/font/xolonium
https://opengameart.org/content/rpgstringsspookys