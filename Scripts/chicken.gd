
extends Area2D

export var facing_left = true
var interface
var sprite
var anim

func _ready():
	interface = get_node("/root/Game/Interface")
	sprite = get_node("Sprite")
	anim = get_node("anim")
	anim.play("idle")
	
	connect("body_enter",self,"_hit")
	
	interface._add_chicken()
	
	if !facing_left:
		sprite.set_scale(Vector2(-1, 1))

func _hit(body):
	
	if (body.get_name() == "player"):
		self.hide()
		self.queue_free()
		interface._remove_chicken()
		AudioPlayer._play_audio("chicken_squawk")