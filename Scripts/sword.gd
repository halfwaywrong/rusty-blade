
extends Node2D

var left
var right

func _ready():
	
	left = get_node("left")
	right = get_node("right")
	
	left.connect("body_enter",self,"on_area_body_enter")
	right.connect("body_enter",self,"on_area_body_enter")
	
func _enable(direction):
	
	if (direction == "left"):
		left.show()
		left.set_layer_mask(1)
		left.set_collision_mask(1)
	elif (direction == "right"):
		right.show()
		right.set_layer_mask(1)
		right.set_collision_mask(1)
	
func _disable():
	left.hide()
	left.set_layer_mask(0)
	left.set_collision_mask(0)
	right.hide()
	right.set_layer_mask(0)
	right.set_collision_mask(0)


func on_area_body_enter(body):
	if body in get_tree().get_nodes_in_group("enemy"):
		body._hit()