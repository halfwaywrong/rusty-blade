
extends RigidBody2D

var WALK_MAX_VELOCITY = 80.0
var WALK_ACCEL = 600.0
var MAX_DISTANCE_T0_PLAYER = 90
var MIN_TIME_BETWEEN_HITS = 1

var alive = true
var health = 3
var anim
var body
var player
var siding_left = false
var player_left = true
var sword
var sword_time = 1e3
var attacking = false

func _ready():
	anim = get_node("anim")
	body = get_node("body")
	player = get_node("../player")
	sword = get_node("sword")
	
	sword._disable()
	

func _integrate_forces(state):
	
	var lv = state.get_linear_velocity()
	var new_siding_left = siding_left
	var step = state.get_step()
	var player_pos = player.get_pos()
	var cur_pos = get_pos()
	
	if (alive):
		
		if (player_pos.x < (cur_pos.x - 16)):
			player_left = true
		elif (player_pos.x > (cur_pos.x + 16)):
			player_left = false
		
		if (cur_pos.distance_to(player_pos) < MAX_DISTANCE_T0_PLAYER):
			if (anim.get_current_animation() == "idle" or anim.get_current_animation() == "walk"):
				if (lv.x > -WALK_MAX_VELOCITY and player_left):
					lv.x -= WALK_ACCEL*step
				elif (lv.x < WALK_MAX_VELOCITY and !player_left):
					lv.x += WALK_ACCEL*step
					
			
			if (!attacking and sword_time > MIN_TIME_BETWEEN_HITS and (randi()%11+1 == 10)):
				attacking = true
				sword_time = 0
				anim.play("hit")
				AudioPlayer._play_audio("skeleton_sword")
			else:
				sword_time += step
		else:
			anim.play("idle")
		
		if (anim.get_current_animation() == "hit" and anim.get_current_animation_pos() == anim.get_current_animation_length()):
			attacking = false
			sword._disable()
		
		if (!anim.get_current_animation() or anim.get_current_animation_pos() == anim.get_current_animation_length()):
			if (lv.x == 0):
				anim.play("idle")
			else:
				anim.play("walk")
			
		# Check siding
		if (lv.x < 0 and anim.get_current_animation() != "hurt"):
			new_siding_left = true
		elif (lv.x > 0 and anim.get_current_animation() != "hurt"):
			new_siding_left = false
			
		# Update siding
		if (new_siding_left != siding_left):
			if (new_siding_left):
				get_node("sprite").set_scale(Vector2(-1, 1))
			else:
				get_node("sprite").set_scale(Vector2(1, 1))
			
			siding_left = new_siding_left
		
		if (attacking):
			if (siding_left):
				sword._enable("left")
			else:
				sword._enable("right")
		else:
			sword._disable()
		
		lv += state.get_total_gravity()*step
		state.set_linear_velocity(lv)
	else:
		lv.x = 0
		lv.y = 0
		lv += state.get_total_gravity()*step
		state.set_linear_velocity(lv)

func _hit():
	health -= 1
	sword._disable()
	
	if (player_left):
		apply_impulse(Vector2(0,0), Vector2(150,0))
	else:
		apply_impulse(Vector2(0,0), Vector2(-150,0))
	
	if (health <= 0):
		alive = false
		anim.play("die")
		AudioPlayer._play_audio("skeleton_death")
		set_collision_mask(2)
		set_layer_mask(2)
	else:
		attacking = false
		AudioPlayer._play_audio("skeleton_hurt")
		anim.play("hurt")
		