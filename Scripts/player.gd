
extends RigidBody2D

var WALK_ACCEL = 600.0
var WALK_DEACCEL = 600.0
var WALK_MAX_VELOCITY = 80.0
var AIR_ACCEL = 400
var AIR_DEACCEL = 400
var JUMP_VELOCITY = 120
var STOP_JUMP_FORCE = 50.0
var MAX_SWORD_TIME = 0.2
var MAX_TIME_BETWEEN_ATTACKS = 0.3
var MAX_FLOOR_AIRBORNE_TIME = 0.15

var siding_left = false
var anim = ""
var sword_time = 1e3
var airborne_time = 1e3
var jumping = false
var stopping_jump = false
var attacking = false
var can_jump = true
var can_attack = true
var sword
var health = 3
var alive = true
var interface
var camera



func _ready():
	interface = get_node("/root/Game/Interface")
	camera = get_node("camera")
	sword = get_node("sword")
	sword._disable()

func _integrate_forces(state):
	
	var move_left = Input.is_action_pressed("move_left")
	var move_right = Input.is_action_pressed("move_right")
	var jump = Input.is_action_pressed("jump")
	var sword_use = Input.is_action_pressed("sword")
	
	var lv = state.get_linear_velocity()
	var step = state.get_step()
	var new_siding_left = siding_left
	var new_anim = anim
	var found_floor = false
	var floor_index = -1
	if (alive):
		
		for x in range(state.get_contact_count()):
			var ci = state.get_contact_local_normal(x)
			if (ci.dot(Vector2(0, -1)) > 0.6):
				found_floor = true
				floor_index = x
		
		if (found_floor):
			airborne_time = 0.0
		else:
			airborne_time += step # Time it spent in the air
			
		var on_floor = airborne_time < MAX_FLOOR_AIRBORNE_TIME
		
		if sword_use and !attacking and can_attack:
			attacking = true
			sword_time = 0
			can_attack = false
		else:
			sword_time += step
		
		if !can_attack and sword_time > MAX_TIME_BETWEEN_ATTACKS and !sword_use:
			can_attack = true
		
		if (!jump):
			can_jump = true
		
		# Process jump
		if (jumping):
			can_jump = false
			if (lv.y > 0):
				# Set off the jumping flag if going down
				jumping = false
			elif (not jump):
				stopping_jump = true
			
			if (stopping_jump):
				lv.y += STOP_JUMP_FORCE*step
			
		
		if (on_floor):
			# Process logic when character is on floor
			if (move_left and not move_right):
				if (lv.x > -WALK_MAX_VELOCITY):
					lv.x -= WALK_ACCEL*step
			elif (move_right and not move_left):
				if (lv.x < WALK_MAX_VELOCITY):
					lv.x += WALK_ACCEL*step
			else:
				var xv = abs(lv.x)
				xv -= WALK_DEACCEL*step
				if (xv < 0):
					xv = 0
				lv.x = sign(lv.x)*xv
			
			
			# Check jump
			if (not jumping and jump and can_jump):
				lv.y = -JUMP_VELOCITY
				jumping = true
				stopping_jump = false
				
				
			if (abs(lv.x) < 0.1):
				if (sword_time > MAX_SWORD_TIME):
					new_anim = "idle"
					attacking = false
					sword._disable()
			else:
				if (sword_time > MAX_SWORD_TIME):
					new_anim = "walk"
					attacking = false
					sword._disable()
			
			if (attacking):
				new_anim = "hit"
		else:
			# Process logic when the character is in the air
			if (move_left and not move_right):
				if (lv.x > -WALK_MAX_VELOCITY):
					lv.x -= AIR_ACCEL*step
			elif (move_right and not move_left):
				if (lv.x < WALK_MAX_VELOCITY):
					lv.x += AIR_ACCEL*step
			else:
				var xv = abs(lv.x)
				xv -= AIR_DEACCEL*step
				if (xv < 0):
					xv = 0
				lv.x = sign(lv.x)*xv
			
			if (lv.y != 0):
				if (sword_time < MAX_SWORD_TIME):
					new_anim = "jump_hit"
				else:
					new_anim = "jump"
					attacking = false
					sword._disable()
		
		
		# Check siding
		if (lv.x < 0 and move_left):
			new_siding_left = true
		elif (lv.x > 0 and move_right):
			new_siding_left = false
			
		# Update siding
		if (new_siding_left != siding_left):
			if (new_siding_left):
				get_node("sprite").set_scale(Vector2(-1, 1))
			else:
				get_node("sprite").set_scale(Vector2(1, 1))
			
			siding_left = new_siding_left
		
		if (attacking):
			if (siding_left):
				sword._enable("left")
			else:
				sword._enable("right")
		
		# Change animation
		if (new_anim != anim):
			anim = new_anim
			get_node("anim").play(anim)
			if (anim == "jump_hit" or anim == "hit"):
				AudioPlayer._play_audio("player_sword")
		
		
		lv += state.get_total_gravity()*step
		state.set_linear_velocity(lv)
	else:
		lv.x = 0
		lv.y = 0
		lv += state.get_total_gravity()*step
		state.set_linear_velocity(lv)
		
		if (get_node("anim").get_current_animation() == "die" and get_node("anim").get_current_animation_pos() == get_node("anim").get_current_animation_length()):
			interface._game_over(false)

func _hit(enemy_side):
	health -= 1
	camera.shake(0.2, 15, 8)
	if (enemy_side == "right"):
		apply_impulse(Vector2(-5,0), Vector2(500,0))
	else:
		apply_impulse(Vector2(5,0), Vector2(-500,0))
	
	if (health <= 0):

		_game_over(false)
	else:
		AudioPlayer._play_audio("player_hurt")
	
	interface._remove_heart(health)
	
func _game_over(win):
	alive = false
	set_collision_mask(2)
	set_layer_mask(2)
	if (win):
		get_node("anim").play("idle")
		interface._game_over(true)
	else:
		get_node("anim").play("die")
		AudioPlayer._play_audio("player_death")
		sword._disable()