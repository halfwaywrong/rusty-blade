
extends CanvasLayer

var path = ""

func fade_to(scn_path):
	self.path = scn_path
	get_node("AnimationPlayer").play("fade")

func change_scene():
	if path != "":
		get_tree().change_scene(path)