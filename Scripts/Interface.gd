
extends CanvasLayer

var player
var heart = load("res://Scenes/heart.tscn")
var hearts = []
var chickens = 0
var chicken_counter
var quit
var restart
var msg_lose
var msg_win
var game_over_panel

func _ready():
	player = get_node("../player")
	chicken_counter = get_node("chicken_counter")
	quit = get_node("game_over_panel/btn_quit")
	restart = get_node("game_over_panel/btn_restart")
	msg_lose = get_node("game_over_panel/msg_lose")
	msg_win = get_node("game_over_panel/msg_win")
	game_over_panel = get_node("game_over_panel")
	_add_hearts()

func _add_hearts():
	var xPos = 16
	for count in range(0, player.health):
		var newHeart = heart.instance()
		newHeart.set_pos(Vector2(xPos, 12))
		hearts.insert(count, newHeart)
		add_child(hearts[count], false)
		xPos += newHeart.get_item_rect().size.x + 5

func _remove_heart(health):
	hearts[health].queue_free()

func _add_chicken():
	chickens += 1
	chicken_counter.set_text("x " + str(chickens))

func _remove_chicken():
	chickens -= 1
	chicken_counter.set_text("x " + str(chickens))
	
	if (chickens == 0):
		player._game_over(true)
		_game_over(true)

func _game_over(win):
	game_over_panel.show()
	quit.show()
	restart.show()
	if (win):
		msg_win.show()
	else:
		msg_lose.show()


func _on_btn_restart_pressed():
	Transition.fade_to("res://Scenes/Game.tscn")

func _on_btn_quit_pressed():
	get_tree().quit()



