
extends SamplePlayer2D

func _ready():
	set_polyphony(5)

func _play_audio(fileName):
	play(fileName)
